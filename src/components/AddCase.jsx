import React, { useState } from 'react';
import { Row, Col, Button, Form} from 'react-bootstrap';
import axios from 'axios';
import { useParams } from 'react-router-dom';

function AddCase (){
    const[message,setMessage] = useState('');
    let { idProject } = useParams();
    const[testID,setTestID] = useState('');
    const[title,setTitle] = useState('');
    const[stepDetails,setStepDetails] = useState('');
    const[expectedResult,setExpectedResult] = useState('');
    const[actualResult,setActualResult] = useState('');
    const[status,setStatus] = useState('');
    const[capture,setCapture] = useState('');
    const[note,setNote] = useState('');
 
    const onChangeTestID = (e) =>{
        const value = e.target.value
        setTestID (value)
    }

    const onChangeTitle = (e) => {
        const value = e.target.value
        setTitle(value)
    }
    const onChangeStepDetails = (e) => {
        const value = e.target.value
        setStepDetails(value)
    }
    const onChangeExpectedResult = (e) => {
        const value = e.target.value
        setExpectedResult(value)
    }
    const onChangeActualResult = (e) => {
        const value = e.target.value
        setActualResult(value)
    }
    const onChangeStatus = (e) => {
        const value = e.target.value
        setStatus(value)
    }
    const onChangeCapture = (e) => {
        const value = e.target.value
        setCapture(value)
    }
    const onChangeNote= (e) => {
        const value = e.target.value
        setNote(value)
    }
    const submitCase = () => {
        const data = {
            testID :testID,
            title : title,
            stepDetails : stepDetails,
            expectedResult : expectedResult,
            actualResult : actualResult,
            status : status,
            idProject : idProject,
            capture : capture,
            note : note

        }
        axios.post(`http://localhost:3000/add-case?id=${idProject}`,data)
        .then(res => {
            console.log(res)
           
        })
        .catch(error => {
            if(error.response){
                setMessage(error.response.data.message);
                alert(message);
            } 
        })  
    }

    return (
        <Form>
        <Row>
            <Col className="px-3 py-1" sm={6}>
                <Form.Group className="mb-3" >
                <Form.Label>Test Id</Form.Label>
                <Form.Control
                         value={testID} 
                         onChange={onChangeTestID}
                    />
                    </Form.Group>
                    <Form.Group className="mb-3" >
                    <Form.Label>Title</Form.Label>
                    <Form.Control
                        value={title} 
                        onChange={onChangeTitle}
                    />
                    </Form.Group>
                    <Form.Group
                    className="mb-3" >
                    <Form.Label>Step Details</Form.Label>
                    <Form.Control 
                    as="textarea" 
                    rows={4} 
                    value={stepDetails} 
                    onChange={onChangeStepDetails} />
                    </Form.Group>
                    <Form.Group className="mb-3" >
                    <Form.Label>Expected Results</Form.Label>
                    <Form.Control
                       value={expectedResult} 
                       onChange={onChangeExpectedResult}
                    />
                    </Form.Group>
                </Col>
        
                <Col className="p-0" sm={6}>
                <Form.Group className="mb-3" >
                    <Form.Label>Actual Results</Form.Label>
                    <Form.Control
                        value={actualResult} 
                        onChange={onChangeActualResult}
                    />
                    </Form.Group>
                    <Form.Label>Status</Form.Label>
                    <Form.Select  
                        value={status} 
                        onChange={onChangeStatus} >
                        <option >Pilih Status</option>
                        <option >Pass</option>
                        <option >Fail</option>
                        <option >Pending</option>
                    </Form.Select>
                    
                    <Form.Group className="mb-3" >
                    <Form.Label>Capture</Form.Label>
                    <Form.Control
                        value={capture} 
                        onChange={onChangeCapture}
                    />
                    </Form.Group>
                    <Form.Group
                    className="mb-3" >
                    <Form.Label>Note</Form.Label>
                    <Form.Control 
                    as="textarea" 
                    rows={4} 
                    value={note} 
                    onChange={onChangeNote} />
                    </Form.Group>
                    </Col>
                    <div className="px-1 py-3" sm={6}>
                <Button variant="primary" type="submit"  onClick={submitCase} >
                            Save Case
                </Button>
       </div>
            </Row>
            </Form>
      
    )
}
export default AddCase;