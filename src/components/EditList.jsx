import { Form, Button } from "react-bootstrap"
import React, {useEffect, useState} from 'react';
import axios from 'axios';

function  EditList (id) {
const [project, setProject] = useState({});
const [message,setMessage] = useState('');

const[projectName,setProjectName] = useState('');
const[deskripsi,setDeskripsi] = useState('');
const[status,setStatus] = useState('');

const findProject = async () => {
    console.log(id.id)
    await axios.post(`http://localhost:3000/find-project?id=${id.id}`)
    .then((data)=> {
        console.log(data)
        setProject(data.data.data)
        setProjectName(data.data.data.projectName)
        setDeskripsi(data.data.data.deskripsi)
        setStatus(data.data.data.status)
        console.log(data.data.data.projectName)
    })
   
  };

  useEffect(() => {
    findProject();
  }, [id]);
    
const UpdateProject = () => {
    const data = {
        projectName : projectName,
        deskripsi : deskripsi,
        status : status
    }
    axios.put(`http://localhost:3000/update-project?id=${id.id}`,data)
    .then(res => {
       
        console.log(res)
        
    })
    .catch(error => {
        if(error.response){
            setMessage(error.response.data.message);
            alert(message);
        } 
    })  
}
     return (
        <Form onSubmit={UpdateProject} >
            <Form.Group className="form-inputs" controlId="projectName">
            <Form.Label>Nama Project</Form.Label>
                <Form.Control
                    type="text"
                    value={projectName}
                    onChange={(e)=> setProjectName(e.target.value)} />
            </Form.Group>
            <Form.Group className="form-inputs" controlId="deskripsi" >
            <Form.Label>Deskripsi</Form.Label>
                <Form.Control
                    type="text"
                    value={deskripsi}
                    onChange={(e)=> setDeskripsi(e.target.value)}/>
            </Form.Group>
            <Form.Label controlId="status">Status</Form.Label>
                <Form.Select 
                            type="text"
                            value={status}
                            onChange={(e)=> setStatus(e.target.value)} >
                            
                            <option value="status">Pilih Status</option>
                            <option value="ongoing">ongoing</option>
                            <option value="completed">completed</option>
                            
                </Form.Select>
                <div className="px-1 py-3" sm={6}>
                <Button variant="success" type="submit"  onClick={UpdateProject}>
                    Edit Project
                </Button>
                </div>      
        </Form>
     );
}
export default EditList;

