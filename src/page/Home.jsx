import { Container , Button, Modal , Card} from 'react-bootstrap';
import Navbar from "../components/Navbar";
import TableList from "../components/TableList";
import AddList from "../components/AddList";
import {BsPlusLg  } from "react-icons/bs";
import React, { useState } from 'react';

function Home () {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return(
        <>
        <Container fluid id="home">
        <Navbar />

        {/* awal cards */}
        <Card className="px-6 py-8 m-4" >
        <Card.Header as='h4' className='d-flex align-items-between justify-content-sm-between'>
            LIST PROJECT
            <Button variant="primary" size="sm" onClick={handleShow} >
                <BsPlusLg /> Add
                </Button>
        </Card.Header>
        <Card.Body>
            {/* awal modals add */}
            <Container>
            <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
            <Modal.Title >Add Project</Modal.Title>
            </Modal.Header>
                <Modal.Body>
                  <AddList />
                </Modal.Body>
          
            </Modal>
        </Container>
                {/* akhir modals add */}

            {/* awal table */}
            <TableList/>
                 
                {/* akhir table */}
            </Card.Body>   
        </Card>
        {/* akhir cards */}
    </Container>
    </>       
  );
}
export default Home;