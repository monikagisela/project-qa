import React from 'react';
import TableScrollbar from 'react-table-scrollbar';
import { useNavigate} from 'react-router-dom';
import { useEffect, useState } from 'react';
import {Table, Modal, Button} from 'react-bootstrap';
import axios from "axios";
import {BsFillPencilFill, BsTrash } from "react-icons/bs";
import EditList from "../components/EditList";

function TableL (){
    const navigate = useNavigate();
    const [show, setShow] = useState(false);
    const [message,setMessage] = useState('');
    const [project, setProject] = useState({});
    
    const [id, setId] = useState({});
    const handleClose = () => setShow(false);
    const handleShow = (id) => {
        setId(id);
        setShow(true);
    }
    
    const getDataProject = async () => {
      await axios.get('http://localhost:3000/view-project')
      .then((data)=> {setProject(data.data)})
        
    };
  
  
    useEffect(() => {
      getDataProject();
     
    }, []);

    const DeleteProject = (id) => {
        axios.post(`http://localhost:3000/delete-project?id=${id}`,)
        .then(res => {
           
            console.log(res)
            
        })
        .catch(error => {
            if(error.response){
                setMessage(error.response.data.message);
                alert(message);
            } 
        })  
        
    }

        
    return (
        <TableScrollbar  >
            <table class="table table-bordered table-striped mb-0">
                <thead  >
                    <tr>
                    
                    <th className="col-2">Nama Project</th>
                    <th className="col-2">Deskripsi</th>
                    <th className="col-1">Status</th>
                    <th className="col-1">Create at</th>
                    <th className="col-1">Update at</th>
                    <th className="col-1">Action</th>
                    
                    </tr>
                </thead>
                <tbody >
                 {project.data?.map(project=>(
                        <tr key={project.projectName}>
                           
                          <td onClick={() =>  navigate(`/TestCase/${project._id}`) } >{project.projectName}</td>
                          <td>{project.deskripsi}</td>
                          <td>{project.status}</td>
                          <td>{project.createdAt}</td>
                          <td>{project.updatedAt}</td>
                          <td>
                            <Button type="button" variant="success" onClick={() => handleShow(project._id) }><BsFillPencilFill /> </Button>
                            <Button type="button" variant="danger" onClick ={() => DeleteProject(project._id)}><BsTrash /></Button>
                            </td>
                        </tr>
                      ))} 

        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>
                    Edit Project
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
             <EditList  id={id}/>
            </Modal.Body>
            
        </Modal>
        </tbody>
        </table>
    </TableScrollbar>
   
    )
}

export default TableL;