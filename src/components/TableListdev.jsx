import React from 'react';
import TableScrollbar from 'react-table-scrollbar';
import { useNavigate} from 'react-router-dom';
import { useEffect, useState } from 'react';
import axios from "axios";

function TableL (){
    const navigate = useNavigate();
    const [project, setProject] = useState({});
 
    const getDataProject = async () => {
      await axios.get('http://localhost:3000/view-project')
        .then((data)=> {setProject(data.data)})
        
    };
  
    useEffect(() => {
      getDataProject();
    }, []);

    return (
        <TableScrollbar >
          <table class="table table-bordered table-striped mb-0">
            <thead  >
              <tr>
              
              <th className="col-2">List Project</th>
              <th className="col-2">Deskripsi</th>
              <th className="col-1">Status</th>
            
              </tr>
            </thead>
            <tbody >   
                 {project.data?.map(project=>(
                
                        <tr key={project.projectName}>
                          
                          <td onClick={() =>  navigate(`/TestCaseDev/${project._id}`) } >{project.projectName}</td>
                          <td>{project.deskripsi}</td>
                          <td>{project.status}</td>
                          
                        </tr>
                      ))}      
        </tbody>
        </table>
    </TableScrollbar>

    
    )
}

export default TableL;