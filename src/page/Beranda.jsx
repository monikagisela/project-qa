import React from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
// import jamil from "./../assets/jamil.jpeg";
import bg1 from "./../assets/bg1.png";

function Beranda(){
    return(
        <>
        <Container fluid id="/">
            <Row className="d-flex align-items-center justify-content-center">
                <Col className="px-5 py-5" sm={6}>
                    <Col xs={6} md={{ span: 8, offset: 2 }} className="" >
                      
                            <div className="d-grid gap-2">
                                <Button variant="primary" type="submit" size="lg" href="login" >
                                    QA
                                </Button>
                                <Button variant="primary" type="submit" size="lg" href="homedev">
                                    DEVELOPER
                                </Button>
                            </div>
                       
                    </Col>
                </Col>
                <Col className="p-0" sm={6}>
                    <img
                        className="vh-100 w-100 d-block"
                        src={bg1}
                    />
                </Col>
            </Row>
           
        </Container>
    </>
    )
}
export default Beranda;