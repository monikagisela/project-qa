import './App.css';
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import Beranda from './page/Beranda';
import Home from './page/Home';
import LoginPage from './page/LoginPage';
import TestCase from './page/TestCase';
import TestCaseDev from './page/TestCaseDev';
import HomeDev from './page/HomeDev';
import TableList from './components/TableList';


function App() {
  return (
    <BrowserRouter>
    <Routes >
        <Route path='/' element={<Beranda/>}/>
        <Route path='/Login' element={<LoginPage/>}/>
        <Route path='/Home' element={<Home/>}/>
        <Route path='/HomeDev' element={<HomeDev/>}/>
        <Route path="/TestCase/:idProject" element={<TestCase/>}/>
        <Route path='/TestCaseDev/:idProject' element={<TestCaseDev/>}/>
        <Route path="/List" element={<TableList />} />
        
    </Routes>
  </BrowserRouter>
  
  );
}

export default App;
