import React from 'react';
import { Container ,Navbar } from 'react-bootstrap';
import Logo from "./../assets/Logo.png";

function NavigasiDev (){
    return(
        <div>
            <Navbar bg="light" >
              <Container fluid>
              <Navbar.Brand >
                    <img clasName="d-flex align-items-center justify-content-start" width="45px"
                            src={Logo}  />
                    </Navbar.Brand>
              </Container>
            </Navbar>
        </div>

    )
}

export default NavigasiDev;