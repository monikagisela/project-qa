import React from 'react';
import { Container ,Navbar, Button} from 'react-bootstrap';
import Logo from "./../assets/Logo.png";
import axios from 'axios';

function Navigasi (){
    const logout= async () => {
    await axios.delete('http://localhost:3000/logout', { withCredentials: true })
      .then(response => {
        console.log(response)
      })
      .catch(error => {
        console.log("logout error", error);
      });
        localStorage.clear();
        window.location.href = '/';

    }
    return(
        <div>
            <Navbar bg="light" expand="top" >
              <Container fluid>
              <Navbar.Brand >
                    <img clasName="d-flex align-items-center justify-content-start" width="45px"
                            src={Logo}  />
                    </Navbar.Brand>
                    <Button
                     variant="primary" 
                     type="keluar" 
                     size="sm" 
                     onClick ={() => logout()}
                     className="d-flex align-items-center justify-content-end">
                            LogOut
                        </Button>

              </Container>
            </Navbar>
        </div>

    )
}

export default Navigasi;