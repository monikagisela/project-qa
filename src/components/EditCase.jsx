import {Row, Col, Form, Button} from 'react-bootstrap';

import React, {useEffect, useState} from 'react';
import axios from 'axios';

function EditCase(id) {
    const [testcase, setTestcase] = useState({});
    const [message,setMessage] = useState('');

    const[testID,setTestID] = useState('');
    const[title,setTitle] = useState('');
    const[stepDetails,setStepDetails] = useState('');
    const[expectedResult,setExpectedResult] = useState('');
    const[actualResult,setActualResult] = useState('');
    const[status,setStatus] = useState('');
    const[capture,setCapture] = useState('');
    const[note,setNote] = useState('');

const findCase = async () => {
    console.log(id.id)
    await axios.get(`http://localhost:3000/find-case?id=${id.id}`)
    .then((data)=> {
        console.log(data)
        setTestcase(data.data.data)
        setTestID(data.data.data.testID)
        setTitle(data.data.data.title)
        setStepDetails(data.data.data.stepDetails)
        setExpectedResult(data.data.data.expectedResult)
        setActualResult(data.data.data.actualResult)
        setStatus(data.data.data.status)
        setCapture(data.data.data.capture)
        setNote(data.data.data.note)
    })
   
  };

  useEffect(() => {
    findCase();
  }, [id]);
    
const UpdateCase = () => {
    const data = {
            testID :testID,
            title : title,
            stepDetails : stepDetails,
            expectedResult : expectedResult,
            actualResult : actualResult,
            status : status,
            capture : capture,
            note : note

    }
    axios.put(`http://localhost:3000/update-case?id=${id.id}`,data)
    .then(res => {
       
        console.log(res)
        
    })
    .catch(error => {
        if(error.response){
            setMessage(error.response.data.message);
            alert(message);
        } 
    })  
}
    return(
        <Form>
        <Row>
            <Col className="px-3 py-1" sm={6}>
                <Form.Group className="mb-3" >
                <Form.Label>Test Id</Form.Label>
                <Form.Control
                         value={testID} 
                         onChange={(e)=> setTestID(e.target.value)}
                    />
                    </Form.Group>
                    <Form.Group className="mb-3" >
                    <Form.Label>Title</Form.Label>
                    <Form.Control
                        value={title} 
                        onChange={(e)=> setTitle(e.target.value)}
                    />
                    </Form.Group>
                    <Form.Group
                    className="mb-3" >
                    <Form.Label>Step Details</Form.Label>
                    <Form.Control 
                    as="textarea" 
                    rows={4} 
                    value={stepDetails} 
                    onChange={(e)=> setStepDetails(e.target.value)} />
                    </Form.Group>
                    <Form.Group className="mb-3" >
                    <Form.Label>Expected Results</Form.Label>
                    <Form.Control
                       value={expectedResult} 
                       onChange={(e)=> setExpectedResult(e.target.value)}
                    />
                    </Form.Group>
                </Col>
        
                <Col className="p-0" sm={6}>
                <Form.Group className="mb-3" >
                    <Form.Label>Actual Results</Form.Label>
                    <Form.Control
                        value={actualResult} 
                        onChange={(e)=> setActualResult(e.target.value)}
                    />
                    </Form.Group>
                    <Form.Label>Status</Form.Label>
                    <Form.Select  
                        value={status} 
                        onChange={(e)=> setStatus(e.target.value)} >
                        <option >Pilih Status</option>
                        <option >Pass</option>
                        <option >Fail</option>
                        <option >Pending</option>
                    </Form.Select>
                    
                    <Form.Group className="mb-3" >
                    <Form.Label>Capture</Form.Label>
                    <Form.Control
                        value={capture} 
                        onChange={(e)=> setCapture(e.target.value)}
                    />
                    </Form.Group>
                    <Form.Group
                    className="mb-3" >
                    <Form.Label>Note</Form.Label>
                    <Form.Control 
                    as="textarea" 
                    rows={4} 
                    value={note} 
                    onChange={(e)=> setNote(e.target.value)} />
                    </Form.Group>
                    </Col>
                    <div className="px-1 py-3" sm={6}>
                <Button variant="primary" type="submit"  onClick={UpdateCase} >
                            Save 
                </Button>
       </div>
            </Row>
            </Form>
       
    );

}
export default EditCase;