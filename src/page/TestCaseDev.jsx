import { Container,Button,Card} from 'react-bootstrap';
import { BsArrowLeft} from "react-icons/bs";
import TableScrollbar from 'react-table-scrollbar';
import Navbardev from "./../components/Navbardev";
import { useParams , useNavigate} from 'react-router-dom';
import axios from "axios";
import React, { useEffect, useState } from 'react';

function TestCaseDev(){
    let { idProject } = useParams();
    const navigate = useNavigate();
    const [testcase, setTestcase] = useState({});

    const getDataTestcase = async () => {
      await axios.get(`http://localhost:3000/view-case?id=${idProject}`)
      
        .then((data)=> {setTestcase(data.data)})

    }

   useEffect(() => {

      getDataTestcase();
    }, []);
    return(
        <>
        <Container fluid id="testcasedev">
            <Navbardev/>
              
            {/* awal cards */}
            <Card className="px-6 py-8 m-4">
                <Card.Header as="h4" className="d-flex align-items-center justify-content-center">
                    TEST CASE
                    </Card.Header>
            <Card.Body>
                {/* awal table */}
                <TableScrollbar >
                    <table class="table table-bordered table-striped mb-0">
                    <thead>
                        <tr>
                        <th>Test ID</th>
                        <th>Title</th>
                        <th>Step Details</th>
                        <th>Expected Results</th>
                        <th>Actual Result</th>
                        <th>Status</th>
                        <th>Capture</th>
                        <th>Note</th>
                        <th>Timestamp</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                 {testcase.data?.map(testcase=>(
                        <tr key={testcase.testID}>
                           
                          <td>{testcase.testID}</td>
                          <td>{testcase.title}</td>
                          <td>{testcase.stepDetails}</td>
                          <td>{testcase.expectedResult}</td>
                          <td>{testcase.actualResult}</td>
                          <td>{testcase.status}</td>
                          <td>{testcase.capture}</td>
                          <td>{testcase.note}</td>
                          <td>{testcase.createdAt}</td>
                         </tr>
                       ))} 

                    </tbody>
                    </table>
                </TableScrollbar>
                {/* akhir table */}
                <Button variant="primary" type="keluar" size="sm" onClick={() =>  navigate(`../HomeDev`) } >
                    <BsArrowLeft/>
                </Button>
        </Card.Body>
        </Card>
        {/* akhir cards */}
    </Container>
    </>
    );
}

export default TestCaseDev;
