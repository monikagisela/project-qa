import React, { useState} from "react";
import { useNavigate} from 'react-router-dom';
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import bg1 from "./../assets/bg1.png";
import axios from 'axios';
import { BsArrowLeft} from "react-icons/bs";

function LoginPage() {
    const navigate = useNavigate();
    const[email,setEmail] = useState('');
    const[password,setPassword] = useState('');
    const[message,setMessage] = useState('');
   
    const onChangeEmail = (e) =>{
        const value = e.target.value
        setEmail(value)
    }

    const onChangePassword = (e) => {
        const value = e.target.value
        setPassword(value)
    }

    const submitLogin = () => {
        const data = {
            email : email,
            password : password
        }
        axios.post('http://localhost:3000/login',data)
        .then(result => {
            console.log(result)
            if(result){
                localStorage.setItem('token', result.data.token)
                
            } 
            navigate("../Home", { replace: true })
        })
        .catch(error => {
            if(error.response){
                setMessage(error.response.data.message);
            }

        })  
     
    }
    return ( 
        <>
        <p>{message}</p>
            <Container fluid id="login">
                <Row className="d-flex align-items-center justify-content-center">
                    <Col className="px-5 py-5" sm={6}>
                        <Col xs={6} md={{ span: 8, offset: 2 }} className="" >
                            
                            <h1>QA Login</h1>
                            <p>Hey, Enter your details to get log in to your account</p>
                            <Form>
                                <Form.Group className="form-inputs" controlId="formBasicEmail">
                                    <Form.Label>Email</Form.Label>
                                    <Form.Control type="email" value={email} onChange={onChangeEmail} />
                                </Form.Group>
                                <Form.Group className="form-inputs" controlId="formBasicPassword">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control type="password" value={password} onChange={onChangePassword} />
                                </Form.Group>
                                <div className="d-grid gap-2 py-3">
                                    <Button variant="primary"  onClick={submitLogin} size="lg">
                                        Login
                                    </Button>
                                </div>
                            </Form>
                            <div className="px-1 py-3" sm={12}>
                                <Button variant="primary" type="keluar" size="sm" href ="/" >
                                <BsArrowLeft/>
                                </Button>
                            </div>
                        </Col>
                    </Col>
                    <Col className="p-0" sm={6}>
                        <img
                            className="vh-100 w-100 d-block"
                            src={bg1}
                        />
                    </Col>
                </Row>        
            </Container>
        </>
        
    );

}
export default LoginPage;