import { Container , Col, Button, Modal , Card} from 'react-bootstrap';
import { BsArrowLeft , BsPlusLg,BsFillPencilFill, BsTrash} from "react-icons/bs";
import TableScrollbar from 'react-table-scrollbar';
import React, { useEffect, useState } from 'react';
import Navbar from "./../components/Navbar";
import AddCase from "./../components/AddCase";
import EditCase from "./../components/EditCase";
import axios from "axios";
import { useParams, useNavigate } from 'react-router-dom';

function TestCase(){
    const navigate = useNavigate();
    const [message,setMessage] = useState('');
    const [show, setShow] = useState(false);
    const [testcase, setTestcase] = useState({});
    const [id, setId] = useState({});
    const handleShow = () => {
       
        setShow(true);
    }
    let { idProject } = useParams();
    const [showEdit, setShowEdit] = useState(false);
    const handleCloseEdit = () => setShowEdit(false);
    const handleShowEdit = (id) => {
        setId(id);
        setShowEdit(true);
    }

    const getDataTestcase = async () => {
      await axios.get(`http://localhost:3000/view-case?id=${idProject}`)
      
        .then((data)=> {setTestcase(data.data)})

    }

   useEffect(() => {

      getDataTestcase();
    }, []);

    const DeleteCase = (id) => {
        axios.post(`http://localhost:3000/delete-case?id=${id}`,)
        .then(res => {
           
            console.log(res)
            
        })
        .catch(error => {
            if(error.response){
                setMessage(error.response.data.message);
                alert(message);
            } 
        })  
        
    }

    return(
        <>
        <Container fluid id="testcase">
            <Navbar/>
        
        {/* awal cards */}
        <Card className="px-6 py-8 m-4">
            <Card.Header as ='h4' className="d-flex align-items-between justify-content-sm-between">
                TEST CASE
                <Button variant="primary" size="sm" onClick={handleShow}>
                        <BsPlusLg /> Add
                            </Button>
            </Card.Header>
                        {/* awal modals */}
                        <Col md={{ span: 11, offset: 11 }}>
                        <Container>
                            <Modal
                                    size="lg"
                                    show={show}
                                    onHide={() => setShow(false)}
                                    aria-labelledby="example-modal-sizes-title-lg">
                            <Modal.Header closeButton>
                            <Modal.Title className="d-flex align-items-center justify-content-center">
                                Add Test Case</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <AddCase />

                                    </Modal.Body>
                            </Modal>
                        </Container>
            </Col>
            {/* akhir modals */}
            <Card.Body>
            <TableScrollbar>
            <table class="table table-bordered table-striped mb-0">
                <thead>
                    <tr>
                    <th className="col-1">Test ID</th>
                    <th className="col-2">Title</th>
                    <th className="col-2">Step Details</th>
                    <th className="col-2">Expected Results</th>
                    <th className="col-2">Actual Result</th>
                    <th className="col-1">Status</th>
                    <th className="col-1">Capture</th>
                    <th className="col-2">Note</th>
                    <th className="col-1">Timestamp</th>
                    <th className="col-2">Action</th>
                    </tr>
                </thead>
                <tbody>
                    
                        {testcase.data?.map(testcase=>(
                                <tr key={testcase.testID}>
                                
                                <td>{testcase.testID}</td>
                                <td>{testcase.title}</td>
                                <td>{testcase.stepDetails}</td>
                                <td>{testcase.expectedResult}</td>
                                <td>{testcase.actualResult}</td>
                                <td>{testcase.status}</td>
                                <td>{testcase.capture}</td>
                                <td>{testcase.note}</td>
                                <td>{testcase.createdAt}</td>

                                <td>
                                    <button type="button" class="btn btn-success" onClick={() => handleShowEdit(testcase._id) }><BsFillPencilFill /> </button>
                                    <button type="button" class="btn btn-danger" onClick ={() => DeleteCase(testcase._id)}><BsTrash /></button>
                                    </td>
                                </tr>
                            ))} 

          <Modal show={showEdit} onHide={handleCloseEdit}>
            <Modal.Header closeButton>
                <Modal.Title>
                    Edit Case
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
             <EditCase  id={id}/>
            </Modal.Body>  
        </Modal>
         </tbody>
         </table>
         </TableScrollbar>
    
                <Button variant="primary" type="keluar" size="sm" onClick={() =>  navigate(`../Home`) } >
                    <BsArrowLeft/>
                </Button>
            </Card.Body>
        </Card>
        {/* akhir cards */}
    </Container>
    </>
    );
}

export default TestCase;
