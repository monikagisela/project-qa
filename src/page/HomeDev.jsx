import { Container ,Card} from 'react-bootstrap';
import Navbardev from "./../components/Navbardev";
import TableListdev from "./../components/TableListdev";

function HomeDev () {

    return(
        <>
        <Container fluid id="homedev"> 
           <Navbardev />
            {/* awal cards */}
            <Card className="px-5 py-2 m-4">
            <Card.Header as="h4" className="d-flex align-items-center justify-content-center" >LIST PROJECT</Card.Header>
            <Card.Body>
                {/* awal table */}
                <TableListdev />
                 
                {/* akhir table */}
        </Card.Body>
        </Card>
        {/* akhir cards */}
    </Container>
    </>    
  );
}
export default HomeDev;