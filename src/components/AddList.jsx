import React, { useState } from 'react';
import axios from 'axios';
import {  Button, Form} from 'react-bootstrap';

function AddList (){
    const[message,setMessage] = useState('');
   
    const[projectName,setProjectName] = useState('');
    const[deskripsi,setDeskripsi] = useState('');
    const[status,setStatus] = useState('');
 
    const onChangeProjectName = (e) =>{
        const value = e.target.value
        setProjectName(value)
    }

    const onChangeDeskripsi = (e) => {
        const value = e.target.value
        setDeskripsi(value)
    }
    const onChangeStatus = (e) => {
        const value = e.target.value
        setStatus(value)

    }
    const submitSave = () => {
        const data = {
            projectName :projectName,
            deskripsi : deskripsi,
            status : status
        }
        axios.post('http://localhost:3000/add-project',data)
        .then(res => {
            console.log(res)
            // if(res){
            //     localStorage.setItem('', res.data.token)
            // }
        })
        .catch(error => {
            if(error.response){
                setMessage(error.response.data.message);
                alert(message);
            } 
        })  
    }

    return (
        <Form >
        <Form.Group className="form-inputs" >
        <Form.Label>Nama Project</Form.Label>
        <Form.Control
                type="text"
                value={projectName} 
                onChange={onChangeProjectName}
        />
        </Form.Group>
        <Form.Group className="form-inputs" >
        <Form.Label>Deskripsi</Form.Label>
        <Form.Control
                type="text"
                value={deskripsi} 
                onChange={onChangeDeskripsi}
        />
        </Form.Group>
        <Form.Label>Status</Form.Label>
        <Form.Select 
                value={status} 
                onChange={onChangeStatus} >
            
            <option value="status">Pilih Status</option>
            <option value="ongoing">ongoing</option>
            <option value="completed">completed</option>
            
            </Form.Select>
       <div className="px-1 py-3" sm={6}>
       <Button variant="primary" type="submit"  onClick={submitSave} href='/Home' >
                            Save 
                </Button>
       </div>
    </Form>


    )
}
export default AddList;